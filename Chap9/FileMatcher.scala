
object FileMatcher {
	private def filesHere = (new java.io.File(".")).listFiles

	def filesEnding(query: String) = 
		for (file <- filesHere; if file.getName.endsWith(query))
			yield file

	// First version, not using scopes.
	// def filesMatching( query: String,
	// 		matcher: (String, String) => Boolean) = {
	// 		for (file <- filesHere; if matcher(file.getName, query))
	// 			yield file

	// }

	// def filesContaining(query: String) =
	// 	filesMatching(query, _.contains(_))

	// Second version, using closures
	def filesMatching( matcher: String => Boolean) =
		// The type of matcher here is a function taking a string
		// argument and returning a Boolean
		for (file <- filesHere; if matcher(file.getName))
			yield file

	def filesContaining( query: String) =
		filesMatching(_.contains(query))
}