name := "Lists"

version := "0.1.0"

scalaVersion := "2.10.3"

description := "Playing around with lists"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.0.RC3" % "test"
)

org.scalastyle.sbt.ScalastylePlugin.Settings

	