
package de.sollander.lists

import org.scalatest.FunSuite
import Sort.msort
// Astrid: 0278-41041

class TestSort extends FunSuite {
	test("the sort should work for empty Lists") {
		val less = (x: Int, y: Int) => (x < y)
		val l1 = List[Int]()
		val l2 = msort(less)(l1)
		assert(l1 == l2, "The lists should be the same")
		assert(l2.isEmpty, "l2 should be empty")
	}

	test("currying should work") {
		val less = (x: Int, y: Int) => (x < y)
		val intSort = msort(less) _
		val l1 = List(3,2,1)
		val sl1 = intSort(l1)
		assert(sl1 == List(1,2,3))
	}

	test("reverse sort should work") {
		val more = (x: Int, y: Int) => (x > y)
		val less = (x: Int, y: Int) => (x < y)
		val intSort = msort(less) _
		val reverseIntSort = msort(more) _
		val l1 = List(1, 5, 6)
		val sl1 = intSort(l1)
		val sl2 = reverseIntSort(sl1)
		val sl3 = intSort(sl2)
		assert(sl1 == sl3)
	}

}