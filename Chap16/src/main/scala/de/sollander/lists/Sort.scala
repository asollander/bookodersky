/*
 *  Sort object with merge sort function
 */
package de.sollander.lists

object Sort extends Application {

  def msort[T](less: (T,T) => Boolean)(xs: List[T]): List[T] = {
    def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
    case (Nil, _) => ys
    case (_, Nil) => xs
    case ((x1 :: xs1), (y1 :: ys1)) => {
      if (less(x1, y1)) { x1 :: merge(xs1, ys) }
      else { y1 :: merge(xs, ys1) }
    }
    }
    val n = xs.length / 2
        if (n == 0) { xs }
        else {
          val (ys,zs) = xs.splitAt(n)
          merge(msort(less)(ys), msort(less)(zs))
        }
  }


}
