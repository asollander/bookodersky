package de.sollander.scala

import scala.language.implicitConversions

object TestRational {
	implicit def intToRational(i: Int)= new Rational(i)
	
	def test( s: String, k: Int, v: Int) {
		println( s + ": " + new Rational(k, v))
	}

	def main(args: Array[String]) {
		val r = new Rational(145,235)
		println("r: " + r)
		val p = new Rational(2,3)
		println("p: " + p)

		val q = r + p
		println( "q (==r+p) = " + q )

		println( "r < p = " + (r lessThan p))
		println( "q < p = " + q.lessThan(p))

		println( "max(r,p) = " + r.max(p) )

		val q1 = new Rational(3)
		test( "Simplifying", 66, 42)
		val q2 = r * p
		println("r * p = " + q2)


		val x = new Rational(1,2)
		val y = new Rational(2,3)
		println("x+y = " + (x + y))
		println("x.+(y) = " + x.+(y))
		println("x+x*y = " + (x + x * y))
		println("(x+x)*y = " + ((x + x) * y))
		println("x+(x*y) = " + (x + (x * y)))

		println("y * 2 = " + (y*2))
		println("2 * y = " + (2*y))



	}
}