
import scala.io.Source

def widthOfLength(s: String) = s.length.toString.length

if (args.length > 0) {
   println("Starting ...")
   val lines = Source.fromFile(args(0)).getLines().toList
//   var maxWidth = 0
//   for(line <- lines ) 
//     maxWidth = maxWidth.max(widthOfLength(line))
//    val maxWidth = 0.max( 
//    	lines.reduceLeft( (a, b) => if ( a.length > b.length ) a else b )
// 	)
    val longestLine = lines.reduceLeft( 
    	(a, b) => if ( a.length > b.length ) a else b 
	)
   val maxWidth  =widthOfLength(longestLine)
   println("maxWidth == " + maxWidth )
   println("lines: " + lines )
   for(line <- lines ) {
     val numSpaces = maxWidth - widthOfLength(line)
     val padding = " " * numSpaces
     println( padding + line.length + " | " + line )
   }
} else     
  Console.err.println("Please enter a filename")