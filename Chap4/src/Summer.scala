import ChecksumAccumulator.calculate

object Summer {
	def main(args: Array[String]) {
	  println("Going through " + args.length + " arguments")
	  for (arg <- args)
	    println( arg + ": " + calculate(arg))
	}
}