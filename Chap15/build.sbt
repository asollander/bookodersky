name := "Elems"

version := "0.1.0"

scalaVersion := "2.10.3"

description := "My little scala project"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.0.RC3" % "test"
)
