import Element.elem

object TestMe {
	def main(args: Array[String]) {
	  println("Testing!!")
	  val a = elem(Array("Hello", "world!"))
	  println("a: [" + a.height + "," + a.width + "] =\n" + a + "\n")
	  val b = elem('Q', 3, 4)
	  println("b:\n" + b)
	  val c = elem(Array("a", "bb", "ccc", "dddd", "eeeee", "ffff", "ggg", "hh", "i"))
	  println("c:\n" + c)
	  val d = elem("hello") above elem("woooooooorld!")
	  println("d:\n" + d)
	  val e = elem(Array("One", "Two")) beside elem("Three")
	  println("e:\n" + e)
	}
  
}