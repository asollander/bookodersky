import Element.elem

object Element {
	private class ArrayElement(val contents: Array[String]) extends Element

	private class LineElement(s: String) extends Element {
		val contents = Array(s)
				override def width = s.length
				override def height = 1
	}

	private class UniformElement( val c: Char, 
			override val width: Int,
			override val height: Int) extends Element {
		private val line = c.toString * width;
		def contents = Array.fill(height)(line) 
	}
	def elem(contents: Array[String]): Element =
			new ArrayElement(contents)
	def elem(chr: Char, width: Int, height: Int): Element =
	new UniformElement(chr, width, height)
	def elem(line: String): Element =
	new LineElement(line)
}

abstract class Element {
	def contents: Array[String]
	def height: Int = contents.length
	def width: Int = if (height == 0) 0 else contents(0).length
	
	def above(that: Element): Element = {
	  if (this.width == that.width)
		elem( this.contents ++ that.contents)
	  else if (this.width < that.width)
		elem( (this widen that.width).contents ++ that.contents)
	  else
		elem( this.contents ++ (that widen this.width).contents)
	}
	
	def beside(that: Element): Element = { 
	  val this1 = this heighten that.height
	  val that1 = that heighten this.height
	  assert(this1.height == that1.height, "Elements have different height")
		elem(
			for (
					(line1, line2) <- this1.contents zip that1.contents
				) yield line1 + line2
		)
	}
	
	def widen(w: Int): Element = 
	  if (w <= width) this
	  else {
	    val left = elem(' ', (w-width) / 2, height)
	    val right = elem(' ', w - width - left.width, height)
	    left beside this beside right
	  } ensuring (w <= _.width)
	
	def heighten(h: Int): Element = 
	  if (h <= height) this
	  else {
	    val top = elem(' ', width, (h-height) / 2)
	    val bot = elem(' ', width, h - height - top.height)
	    top above this above bot
	  }
	
	override def toString = contents mkString "\n"
}





